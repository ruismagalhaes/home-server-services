# Virtual Box Installation

**IMPORTANT NOTE** : I'm learning all this stuff at the same time that I'm writting this guide...So, some of this information is probably not the best way to achieve the results pointed out here. I'm just someone that loves to learn new stuff :D

# Table of contents

[[_TOC_]] 

# Steps

* To install virtual box 6.1 on Ubuntu server you need to add the following line to your 
/etc/apt/sources.list replacing "mydist" with the ubuntu distribution

```
deb [arch=amd64] https://download.virtualbox.org/virtualbox/debian <mydist> contrib
```

* Download the apt-secure key:

```bash
sudo wget https://www.virtualbox.org/download/oracle_vbox_2016.asc
```

* Add these keys with:

```bash
sudo apt-key add oracle_vbox_2016.asc
```

* To install VirtualBox and extension pack:

```bash
sudo apt-get update
sudo apt-get install virtualbox-6.1
wget https://download.virtualbox.org/virtualbox/6.1.14/Oracle_VM_VirtualBox_Extension_Pack-6.1.14.vbox-extpack
sudo VBoxManage extpack install --replace "extension-pack-name"
sudo VBoxManage extpack cleanup
VBoxManage list extpacks
```

# vboxmanage useful commands

1. List virtual machines

```bash
vboxmanage list vms
```

2. List running virtual machines

```bash
vboxmanage list runningvms
```

3. List info about some virtual machine

```bash
vboxmanage showvminfo "VM name"
```

# sources

http://hadenpike.net/2017/01/24/how-to-use-the-virtualbox-command-line.html

https://www.virtualbox.org/manual/ch01.html#gui-createvm

https://www.virtualbox.org/manual/ch03.html#settings-motherboard

https://www.virtualbox.org/manual/ch08.html



