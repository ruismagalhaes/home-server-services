
# VBoxManage ubuntu-server VM created

**IMPORTANT NOTE** : I'm learning all this stuff at the same time that I'm writting this guide...So, some of this information is probably not the best way to achieve the results pointed out here. I'm just someone that loves to learn new stuff :D

# Table of contents

[[_TOC_]] 

## Steps for creating the VM

1. Create the VM changing the "--name" and the "--basefolder" parameter to one of your choice

```bash
vboxmanage createvm --name "self-hosted-services" --register --basefolder /home/rmagalhaes/vm-folders/self-hosted-services
```

2. Set memory to 6gb and network

```bash
VBoxManage modifyvm self-hosted-services --ioapic on
VBoxManage modifyvm self-hosted-services --memory 6144
VBoxManage modifyvm self-hosted-services --accelerate3d on --accelerate2dvideo on
vboxmanage modifyvm self-hosted-services --firmware efi
vboxmanage modifyvm self-hosted-services --nic1 bridged --bridgeadapter1 enp37s0
vboxmanage modifyvm self-hosted-services --hwvirtex on
vboxmanage modifyvm self-hosted-services --graphicscontroller vmsvga
```

**NOTE:** In order to get the active network interface run the following:

```bash
ifconfig | awk -F: '/^en/ { print $1 }'
```

3. Create the Disk with 800GB and connect the CD ISO

```bash
VBoxManage createhd --filename /home/rmagalhaes/vm-folders/self-hosted-services/self-hosted-services/self_hosted_services_DISK.vdi --size 819200 --format VDI
VBoxManage storagectl self-hosted-services --name "SATA Controller" --add sata --controller IntelAhci
VBoxManage storageattach self-hosted-services --storagectl "SATA Controller" --port 0 --device 0 --type hdd --medium /home/rmagalhaes/vm-folders/self-hosted-services/self-hosted-services/self_hosted_services_DISK.vdi
VBoxManage storagectl self-hosted-services --name "IDE Controller" --add ide --controller PIIX4
VBoxManage storageattach self-hosted-services --storagectl "IDE Controller" --port 1 --device 0 --type dvddrive --medium /home/rmagalhaes/vm-folders/self-hosted-services/self-hosted-services/ubuntu-server.iso
VBoxManage modifyvm self-hosted-services --boot1 dvd --boot2 disk --boot3 none --boot4 none
```

4. Enable RDP

```bash
VBoxManage modifyvm self-hosted-services --vrde on
VBoxManage modifyvm self-hosted-services --vrdemulticon on --vrdeport 10002
```
5. Disable audio to avoid error:

```bash
VBoxManage modifyvm self-hosted-services --audio none
```

6. Change the cpu number to 4 and Start the VM

```bash
VBoxManage modifyvm self-hosted-services --cpus 4
VBoxHeadless --startvm self-hosted-services
```

OR

```bash
VBoxManage modifyvm self-hosted-services --cpus 4
VBoxManage startvm self-hosted-services --type headless
```

**NOTE:** This way of starting the VM helps 
troubleshooting problems reported by VBoxManage startvm, because you can sometimes see more detailed error messages, especially for early failures before the VM execution is started. In normal situations VBoxManage startvm is preferred, since it runs the VM directly as a background process which has to be done explicitly when directly starting with VBoxHeadless.

7. Connect to the VM trought a RDP software (microsoft Remote desktop is one example). The IP is the Host one (not the VM one) on the 10001 port ([HOST IP][VRDEPORT]). Follow the steps to install ubuntu server and afterwards turn off the VM, change the boot priority and unmount the ubunutu-server.iso (don't forget to install Open-ssh when the setup ask you to do it) :

```bash
vboxmanage controlvm self-hosted-services poweroff --type headless
VBoxManage modifyvm self-hosted-services --boot1 disk --boot2 dvd --boot3 none --boot4 none
VBoxManage storageattach self-hosted-services --storagectl "IDE Controller" \
--port 1 --device 0 --type dvddrive --medium emptydrive
``` 

8. Let's turn off the RDP access because we will always access this VM throught ssh:

```bash
VBoxManage modifyvm self-hosted-services --vrde off --vrdemulticon off
VBoxManage startvm self-hosted-services --type headless
```

9. connect to your new sefl-hosted-services ubuntu server and update and upgrade it:

```bash
ssh <username>@<VM ip>
sudo apt-get -y update
sudo apt-get -y full-upgrade
```
## Create a systemd service

1. Let’s create a file called **/etc/systemd/system/self-hosted-serices.service** :

```
[Unit]
Description=ubuntu virtualbox Vm for self hosted services
After=network.target vboxdrv.service
Before=runlevel2.target shutdown.target

[Service]
User=rmagalhaes
Group=rmagalhaes
Type=forking
Restart=always
RestartSec=5
TimeoutSec=5min
IgnoreSIGPIPE=no
KillMode=process
GuessMainPID=no
RemainAfterExit=yes

ExecStart=/usr/bin/VBoxManage startvm self-hosted-services --type headless
ExecStop=/usr/bin/VBoxManage controlvm self-hosted-services poweroff --type headless

SuccessExitStatus=1

[Install]
WantedBy=multi-user.target
```

2. Reload you daemon and enable your service:

```bash
sudo systemctl daemon-reload
sudo systemctl enable self-hosted-services.service
```

2. To sart the service:

```bash
sudo systemctl start self-hosted-services.service
```

3. To stop the service:

```bash
sudo systemctl stop self-hosted-services.service
```

4. To check the status of the service:

```bash
sudo systemctl status self-hosted-services.service
```

## vboxmanage useful commands

1. List virtual machines

```bash
vboxmanage list vms
```

2. List running virtual machines

```bash
vboxmanage list runningvms
```

3. List info about some virtual machine

```bash
vboxmanage showvminfo self-hosted-services
```
