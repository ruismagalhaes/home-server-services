# VBoxManage ubuntu VM created

**IMPORTANT NOTE** : I'm learning all this stuff at the same time that I'm writting this guide...So, some of this information is probably not the best way to achieve the results pointed out here. I'm just someone that loves to learn new stuff :D

# Table of contents

[[_TOC_]] 

## Steps for creating the VM

**NOTE**: from my experience, ubuntu 20.04 does not run smoothly. It's better to install linux mint for better performance. I have an [available guide](google.com) too.

1. Create the VM changing the "--name" and the "--basefolder" parameter to one of your choice

```bash
VBoxManage createvm --name ubuntu-Catarina --ostype "Ubuntu_64" --register --basefolder /home/rmagalhaes/vm-folders/ubuntu-Catarina
```

2. Set memory to 3gb and network

```bash
VBoxManage modifyvm ubuntu-Catarina --ioapic on                     
VBoxManage modifyvm ubuntu-Catarina --memory 3072 --vram 256
VBoxManage modifyvm ubuntu-Catarina --accelerate3d on --accelerate2dvideo on
vboxmanage modifyvm ubuntu-Catarina --firmware efi
vboxmanage modifyvm ubuntu-Catarina --nic1 bridged --bridgeadapter1 enp37s0
vboxmanage modifyvm ubuntu-Catarina --hwvirtex on
vboxmanage modifyvm ubuntu-Catarina --graphicscontroller vmsvga
```

**NOTE:** In order to get the active network interface run the following:

```bash
ifconfig | awk -F: '/^en/ { print $1 }'
```

3. Create the Disk with 200GB and connect the CD ISO

```bash
VBoxManage createhd --filename /home/rmagalhaes/vm-folders/ubuntu-Catarina/ubuntu-Catarina/ubuntu_Catarina_DISK.vdi --size 204800 --format VDI
VBoxManage storagectl ubuntu-Catarina --name "SATA Controller" --add sata --controller IntelAhci
VBoxManage storageattach ubuntu-Catarina --storagectl "SATA Controller" --port 0 --device 0 --type hdd --medium /home/rmagalhaes/vm-folders/ubuntu-Catarina/ubuntu-Catarina/ubuntu_Catarina_DISK.vdi
VBoxManage storagectl ubuntu-Catarina --name "IDE Controller" --add ide --controller PIIX4
VBoxManage storageattach ubuntu-Catarina --storagectl "IDE Controller" --port 1 --device 0 --type dvddrive --medium /home/rmagalhaes/vm-folders/ubuntu-Catarina/ubuntu-Catarina/ubuntu.iso
VBoxManage modifyvm ubuntu-Catarina --boot1 dvd --boot2 disk --boot3 none --boot4 none
```

4. Enable RDP

```bash
VBoxManage modifyvm ubuntu-Catarina --vrde on
VBoxManage modifyvm ubuntu-Catarina --vrdemulticon on --vrdeport 10001
```
5. Disable audio to avoid error:

```bash
VBoxManage modifyvm ubuntu-Catarina --audio none
```

6. Change the cpu number to 2 and Start the VM

```bash
VBoxManage modifyvm ubuntu-Catarina --cpus 2
VBoxHeadless --startvm ubuntu-Catarina
```

OR

```bash
VBoxManage modifyvm ubuntu-Catarina --cpus 2
VBoxManage startvm ubuntu-Catarina --type headless
```

**NOTE:** This way of starting the VM helps 
troubleshooting problems reported by VBoxManage startvm, because you can sometimes see more detailed error messages, especially for early failures before the VM execution is started. In normal situations VBoxManage startvm is preferred, since it runs the VM directly as a background process which has to be done explicitly when directly starting with VBoxHeadless.

7. Connect to the VM trought a RDP software (microsoft Remote desktop is one example). The IP is the Host one (not the VM one) on the 10001 port ([HOST IP][VRDEPORT]). Follow the steps to install ubuntu desktop and afterwards turn off the VM, change the boot priority and unmount the ubunutu.iso :

```bash
vboxmanage controlvm ubuntu-Catarina poweroff --type headless
VBoxManage modifyvm ubuntu-Catarina --boot1 disk --boot2 dvd --boot3 none --boot4 none
VBoxManage storageattach ubuntu-Catarina --storagectl "IDE Controller" \
--port 1 --device 0 --type dvddrive --medium emptydrive
``` 

8. The VM is now ready for the GUEST ADDITIONS. To download it (can't run any VM at the time of installation):

```bash
apt-get install virtualbox-guest-additions-iso
cp /usr/share/virtualbox/VBoxGuestAdditions.iso /home/rmagalhaes/vm-folders/ubuntu-Catarina/ubuntu-Catarina/
```

9. Now, let's attach the VBoxGuestAdditions.iso as dvddrive

```bash
vboxmanage storageattach ubuntu-Catarina --storagectl "IDE Controller" --port 1 --device 0 --type dvddrive --medium /home/rmagalhaes/vm-folders/ubuntu-Catarina/ubuntu-Catarina/VBoxGuestAdditions.iso
```

10. Start the VM again

```bash
VBoxManage startvm ubuntu-Catarina --type headless
```

11. On the GUEST OS (VM) Install GCC on your VM and reboot it:

```bash
apt-get update
apt-get upgrade
apt-get install build-essential gcc make perl dkms
```

12. On the Host machine, turn it off and on:

```bash
vboxmanage controlvm ubuntu-Catarina poweroff --type headless
VBoxManage startvm ubuntu-Catarina --type headless
```

13. On the GUEST OS (VM) mount the DVD drive and install the VBoxLinuxAdditions:

```bash
cd /media/<linux_user>/Vbox_GAs_6.1.10
./autorun.sh
```

14. On the host machine, unmount the VboxLinuxAdditions cd

```bash
vboxmanage controlvm ubuntu-Catarina poweroff --type headless
VBoxManage storageattach ubuntu-Catarina --storagectl "IDE Controller" \
--port 1 --device 0 --type dvddrive --medium emptydrive
VBoxManage startvm ubuntu-Catarina --type headless
```

15. You are now able to connect to the VM and work with a ubuntu desktop !!


# sources

http://hadenpike.net/2017/01/24/how-to-use-the-virtualbox-command-line.html

https://www.virtualbox.org/manual/ch01.html#gui-createvm

https://www.virtualbox.org/manual/ch03.html#settings-motherboard

https://www.virtualbox.org/manual/ch08.html

https://blog.oracle48.nl/installing-virtualbox-guest-additions-using-the-command-line/

https://help.ubuntu.com/community/VirtualBox/GuestAdditions
