# VBoxManage linux mint xfce VM created

This VM is for Catarina practice her coding and linux skills!

**IMPORTANT NOTE** : I'm learning all this stuff at the same time that I'm writting this guide...So, some of this information is probably not the best way to achieve the results pointed out here. I'm just someone that loves to learn new stuff :D

# Table of contents

[[_TOC_]] 

## Steps for creating the VM

1. Create the VM changing the "--name" and the "--basefolder" parameter to one of your choice

```bash
VBoxManage createvm --name mint-Catarina --register --basefolder /home/rmagalhaes/vm-folders/mint-Catarina
```

2. Set memory to 2gb and network

```bash
VBoxManage modifyvm mint-Catarina --ioapic on
VBoxManage modifyvm mint-Catarina --memory 2048 --vram 128
VBoxManage modifyvm mint-Catarina --accelerate3d on --accelerate2dvideo on
vboxmanage modifyvm mint-Catarina --firmware efi
vboxmanage modifyvm mint-Catarina --nic1 bridged --bridgeadapter1 enp37s0
vboxmanage modifyvm mint-Catarina --hwvirtex on
vboxmanage modifyvm mint-Catarina --graphicscontroller vmsvga
```

**NOTE:** In order to get the active network interface run the following:

```bash
ifconfig | awk -F: '/^en/ { print $1 }'
```

3. Create the Disk with 200GB and connect the CD ISO

```bash
VBoxManage createhd --filename /home/rmagalhaes/vm-folders/mint-Catarina/mint-Catarina/mint_Catarina_DISK.vdi --size 204800 --format VDI
VBoxManage storagectl mint-Catarina --name "SATA Controller" --add sata --controller IntelAhci
VBoxManage storageattach mint-Catarina --storagectl "SATA Controller" --port 0 --device 0 --type hdd --medium /home/rmagalhaes/vm-folders/mint-Catarina/mint-Catarina/mint_Catarina_DISK.vdi
VBoxManage storagectl mint-Catarina --name "IDE Controller" --add ide --controller PIIX4
VBoxManage storageattach mint-Catarina --storagectl "IDE Controller" --port 1 --device 0 --type dvddrive --medium /home/rmagalhaes/vm-folders/mint-Catarina/mint-Catarina/linuxmint-20-xfce-64bit.iso
VBoxManage modifyvm mint-Catarina --boot1 dvd --boot2 disk --boot3 none --boot4 none
```

4. Enable RDP

```bash
VBoxManage modifyvm mint-Catarina --vrde on
VBoxManage modifyvm mint-Catarina --vrdemulticon on --vrdeport 10002
```
5. Disable audio to avoid error:

```bash
VBoxManage modifyvm mint-Catarina --audio none
```

6. Change the cpu number to 2 and Start the VM

```bash
VBoxManage modifyvm mint-Catarina --cpus 2
VBoxHeadless --startvm mint-Catarina
```

OR

```bash
VBoxManage modifyvm mint-Catarina --cpus 2
VBoxManage startvm mint-Catarina --type headless
```

**NOTE:** This way of starting the VM helps 
troubleshooting problems reported by VBoxManage startvm, because you can sometimes see more detailed error messages, especially for early failures before the VM execution is started. In normal situations VBoxManage startvm is preferred, since it runs the VM directly as a background process which has to be done explicitly when directly starting with VBoxHeadless.

7. Connect to the VM trought a RDP software (microsoft Remote desktop is one example). The IP is the Host one (not the VM one) on the 10001 port ([HOST IP][VRDEPORT]). Follow the steps to install ubuntu desktop and afterwards turn off the VM, change the boot priority and unmount the ubunutu.iso :

```bash
vboxmanage controlvm mint-Catarina poweroff --type headless
VBoxManage modifyvm mint-Catarina --boot1 disk --boot2 dvd --boot3 none --boot4 none
VBoxManage storageattach mint-Catarina --storagectl "IDE Controller" \
--port 1 --device 0 --type dvddrive --medium emptydrive
``` 

8. The VM is now ready for the GUEST ADDITIONS. To download it (can't run any VM at the time of installation):

```bash
apt-get install virtualbox-guest-additions-iso
cp /usr/share/virtualbox/VBoxGuestAdditions.iso /home/rmagalhaes/vm-folders/mint-Catarina/mint-Catarina/
```

9. Now, let's attach the VBoxGuestAdditions.iso as dvddrive

```bash
vboxmanage storageattach mint-Catarina --storagectl "IDE Controller" --port 1 --device 0 --type dvddrive --medium /home/rmagalhaes/vm-folders/mint-Catarina/mint-Catarina/VBoxGuestAdditions.iso
```

10. Start the VM again

```bash
VBoxManage startvm mint-Catarina --type headless
```

11. On the GUEST OS (VM) Install GCC on your VM and reboot it:

```bash
sudo apt-get purge virtualbox*
cd /media/<linux_user>/Vbox_GAs_6.1.10
sudo bash ./VBoxLinuxAdditions.run
```

12. On the Host machine, turn it off and on:

```bash
vboxmanage controlvm mint-Catarina poweroff --type headless
VBoxManage startvm mint-Catarina --type headless
```

13. You are now able to connect to the VM and work with a Linux mint desktop !!

## Creating a systemd service for this VM

1. Let’s create a file called **/etc/systemd/system/mint-Catarina.service** :

```
[Unit]
Description=linux mint virtualbox Vm
After=network.target vboxdrv.service
Before=runlevel2.target shutdown.target

[Service]
User=rmagalhaes
Group=rmagalhaes
Type=forking
Restart=no
TimeoutSec=5min
IgnoreSIGPIPE=no
KillMode=process
GuessMainPID=no
RemainAfterExit=yes

ExecStart=/usr/bin/VBoxManage startvm mint-Catarina --type headless
ExecStop=/usr/bin/VBoxManage controlvm mint-Catarina poweroff --type headless

SuccessExitStatus=1

[Install]
WantedBy=multi-user.target
```

**NOTE**: This service will not start automatically or getting restarted. it's just a simple way of starting and powering off the VM

2. Reload you daemon:

```bash
sudo systemctl daemon-reload
```

2. To sart the service:

```bash
sudo systemctl start mint-Catarina.service 
```

3. To stop the service:

```bash
sudo systemctl stop mint-Catarina.service 
```

4. To check the status of the service:

```bash
sudo systemctl status mint-Catarina.service 
```

# sources

http://hadenpike.net/2017/01/24/how-to-use-the-virtualbox-command-line.html

https://www.virtualbox.org/manual/ch01.html#gui-createvm

https://www.virtualbox.org/manual/ch03.html#settings-motherboard

https://www.virtualbox.org/manual/ch08.html

https://blog.oracle48.nl/installing-virtualbox-guest-additions-using-the-command-line/

https://help.ubuntu.com/community/VirtualBox/GuestAdditions

https://medium.com/@benmorel/creating-a-linux-service-with-systemd-611b5c8b91d6
