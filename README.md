
README NOT UPDATED!!! EVERYTHING IS DIFFERENT NOW



# Self Hosted Services

Right now we have 2 virtual machines inside our ubuntu server:

1. linux mint for code development of Catarina

2. Home assistant Hassio VM. (this one has a repo for himself)

All the other self hosted services are hosted on the server itself!

I'm still working on this to add more services and functionalities!

I started by assembling all the components to build my home server (I should write about all the assemble process! Maybe one day...)
After that I downloaded ubuntu server 20.04 and flashed it to a USB and installed it to the home server.
Finally, I've set up the network (with a static IP) of that machine as the following:

| Name          | Value            |
|---------------|------------------|
| Subnet        | 192.168.1.0/24   |
| ip            | 192.168.1.14/24  |
| DNS server    | 8.8.8.8          | 
| Gateway       | 192.168.1.1      |

## Table of services

For now, the services running are the following:

| Service   			| Port  |url			    | Documentation               |
|-------------------------------|-------|---------------------------|-----------------------------|
| heimdall-dashboard      	| 8080  | http://192.168.1.14:8080  |https://heimdall.site/       |
| Portainer			| 8081  | http://192.168.1.14:8081  |https://www.portainer.io/    | 
| code-server-catarina         	| 8082  | http://192.168.1.14:8082  |https://coder.com/           |


# Getting started

## Pre-requisites

You need to have: 

- Docker installed
- An .ssh key accessible on ./code-servers/catarina/.ssh/ if you want to have git features accessible on your code-server
- A script in ./bashScripts/ to set up your development environment with everything that you want

## Configuration

The only thing that you need to do is:

```bash
docker-compose -f docker-compose.services.yml up -d
```

**NOTE:** If you don't want to keep any of the data that I committed here, just delete the ./portainer, ./heimdall, ./code-servers folders

## Future work

Plex, VPN, Traefik, Bookstack, Grafana


